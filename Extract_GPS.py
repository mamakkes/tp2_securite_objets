import csv
import struct


def read_blocks_and_convert(file_path, csv_file_path):
    with open(file_path, 'rb') as file:
        file_content = file.read()

    end_marker = b'\x53\x54\x4F\x50'
    end_index = file_content.find(end_marker)
    content_to_process = file_content if end_index == -1 else file_content[:end_index]

    blocks = []
    start_index = 0

    while True:
        block_start = content_to_process.find(b'\xEF', start_index)
        if block_start == -1 or block_start + 49 > len(content_to_process):
            break

        if block_start + 49 <= len(content_to_process):
            block_data = content_to_process[block_start:block_start + 49]
            lat_bytes = block_data[26:30]
            lon_bytes = block_data[30:34]
            lat = struct.unpack('<i', lat_bytes)[0]
            lon = struct.unpack('<i', lon_bytes)[0]
            lat_dec = lat / 10 ** 7
            lon_dec = lon / 10 ** 7
            blocks.append((lat_dec, lon_dec))
            start_index = block_start + 49
        else:
            break

    # Écrire les coordonnées dans un fichier CSV
    with open(csv_file_path, 'w', newline='') as csvfile:
        fieldnames = ['latitude', 'longitude']
        writer = csv.DictWriter(csvfile, fieldnames=fieldnames)

        writer.writeheader()
        for block in blocks:
            writer.writerow({'latitude': block[0], 'longitude': block[1]})

    return blocks


# Demander à l'utilisateur de saisir le chemin du fichier à traiter
file_path = input("Veuillez entrer le nom du fichier à traiter : ")
csv_file_path = f'{file_path}.csv'

read_blocks_and_convert(file_path, csv_file_path)
print(f"Les coordonnées ont été écrites dans {csv_file_path}")
