
# Extracteur et Convertisseur de Données GPS

Ce script Python extrait les données GPS à partir de fichiers binaires et les convertit au format CSV. Conçu pour traiter des blocs de données spécifiques dans des fichiers binaires, il extrait les informations de latitude et de longitude et sauvegarde ces coordonnées dans un fichier CSV pour faciliter l'analyse et la visualisation.

## Fonctionnalités

- **Traitement de Données Binaires** : Lit les fichiers binaires pour trouver les blocs de données GPS.
- **Extraction de Données GPS** : Extrait les informations de latitude et de longitude des blocs de données.
- **Sortie CSV** : Convertit les données GPS extraites au format de fichier CSV, avec des colonnes pour la latitude et la longitude.
- **Détection de Marqueur de Fin** : Arrête la lecture du fichier binaire lors de la détection d'un marqueur de fin spécifique, assurant que seules les données pertinentes sont traitées.

## Comment Utiliser

1. **Préparez Votre Fichier Binaire** : Assurez-vous d'avoir un fichier binaire contenant des blocs de données GPS. Le script s'attend à ce que ces blocs commencent par un marqueur de byte spécifique.

2. **Exécutez le Script** : Exécutez le script dans votre environnement Python. Lorsqu'il vous sera demandé, entrez le chemin vers votre fichier binaire.

    ```
    python Extract_GPS.py
    ```

3. **Entrez le Chemin du Fichier** : Lorsqu'il vous sera demandé, saisissez le chemin vers le fichier binaire que vous souhaitez traiter.

    ```
    Veuillez entrer le nom du fichier à traiter : T0
    ```

4. **Vérifiez la Sortie** : Le script traite le fichier binaire, extrait les coordonnées GPS et les sauvegarde dans un fichier CSV nommé d'après votre fichier d'entrée mais avec une extension `.csv`.

## Exigences

- Python 3.10
- Aucune bibliothèque Python supplémentaire n'est requise au-delà de `csv` et `struct`, qui font partie de la bibliothèque standard.

## Format de Sortie

Le fichier CSV de sortie contient deux colonnes :
- `latitude` : Latitude du point de données GPS en degrés décimaux.
- `longitude` : Longitude du point de données GPS en degrés décimaux.

## Licence

Ce script est fourni "tel quel", sans garantie d'aucune sorte, expresse ou implicite. Libre pour utilisation, modification et distribution.

---

Profitez de l'extraction et de l'analyse de vos données GPS !
