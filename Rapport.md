# Rapport de Sécurité - Analyse et Extraction de Données GPS

Introduction
------------

Ce rapport détaille la démarche adoptée pour analyser les fichiers binaires et extraire les données GPS contenues, en vue d'améliorer la compréhension et la sécurité des objets concernés.

Analyse Préliminaire
--------------------

La première étape a consisté à ouvrir les fichiers T0, T10, T11 et T13 avec l'outil imhex, dans le but de comprendre la structure et le contenu de ces fichiers. Cette analyse préliminaire a permis d'identifier des informations clés telles que la version du firmware et la présence d'un en-tête spécifique.

Identification des Blocs de Données
-----------------------------------

L'observation des patterns récurrents dans les fichiers a révélé que les blocs de données commencent par le marqueur `EF` et se terminent juste avant un nouveau `EF`, formant ainsi des blocs de 49 octets pour chaque trame.

Extraction des Coordonnées GPS
------------------------------

Grâce à un exemple fourni par le professeur dans le fichier T0, il a été possible de déterminer que les octets 26 à 30 contiennent la latitude et les octets 30 à 34 la longitude. Cette structure a été formalisée dans imHex comme suit :

```
struct dwilen{
    u16 versionjsp;
    char fffff[256];
    char versionFirmware[9];
    char jsp[81];
    char fff[177];
    char entete[60];
};

struct frame{
    char trame1[49];
};

frame dataframe[454] @ 0x248;
dwilen dwilen_at_0x00 @ 0x00;
s32 latitude @ 0x667;
s32 longitude @ 0x66B;
```

Automatisation de l'Extraction
------------------------------

Avec cette compréhension, un script a été développé pour automatiser l'extraction des coordonnées GPS à partir du bloc 240, en commençant à l'octet `EF` et en s'arrêtant à `STOP` en hexadécimal.

Visualisation des Données
-------------------------

Les données extraites ont été sauvegardées dans un fichier CSV, puis utilisées sur le site https://www.gpsvisualizer.com/map_input?form=data pour visualiser les coordonnées sur une carte.
![Visualisation GPS](GPS.png)

Conclusion
----------

Cette démarche méthodique d'analyse et d'extraction des données GPS à partir de fichiers binaires souligne l'importance de la compréhension approfondie des structures de données pour la sécurité et l'analyse des objets connectés.

